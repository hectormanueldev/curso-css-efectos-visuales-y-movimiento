# Curso de CSS Efectos Visuales y Movimiento

## Temario

1. :heavy_plus_sign: Sombra a las cajas
1. :heavy_plus_sign: Sombra a los textos
1. :heavy_plus_sign: Sombras múltiples
1. :heavy_plus_sign: Filtro de sombras
1. :heavy_plus_sign: Degradados lineales
1. :heavy_plus_sign: Degradados radiales
1. :heavy_plus_sign: Degradados cónicos
1. :heavy_plus_sign: Patrones con degradados
1. :heavy_plus_sign: Graficas con degradados
1. :heavy_plus_sign: Filtros
1. :heavy_plus_sign: Filtros múltiples
1. :heavy_plus_sign: Filtros a fondos
1. :heavy_plus_sign: Modo dark light con filtros
1. :heavy_plus_sign: Modos de mezcla
1. :heavy_plus_sign: Modos de mezcla a fondos
1. :heavy_plus_sign: Mascaras clip path
1. :heavy_plus_sign: Formas shape outside
1. :heavy_plus_sign: Sitios one page scroll
1. :heavy_plus_sign: Ajuste de scroll efecto slides verticales
1. :heavy_plus_sign: Ajuste de scroll efecto carrusel horizontal
1. :heavy_plus_sign: Texto con degradado
1. :heavy_plus_sign: Transiciones
1. :heavy_plus_sign: Transformaciones
1. :heavy_plus_sign: Animaciones
1. :heavy_plus_sign: Efecto fadein fadeout
1. :heavy_plus_sign: Efecto shake
1. :heavy_plus_sign: Efecto pulse
1. :heavy_plus_sign: Spinner loader animado
1. :heavy_plus_sign: Botones animados
1. :heavy_plus_sign: Menú de pestañas
1. :heavy_plus_sign: Menú hamburguesa
1. :heavy_plus_sign: Ventana modal
1. :heavy_plus_sign: Intro películas star wars